package db

import (
	"github.com/goonode/mogo"
	"goMovieApp/entity"
	"goMovieApp/utils"

	"log"
)

var mongoConnection *mogo.Connection = nil

//GetConnection is for get mongo connection
func GetConnection() *mogo.Connection {
	if mongoConnection == nil {
		connectionString := utils.EnvVar("DB_CONNECTION_STRING", "")
		dbName := utils.EnvVar("DB_NAME", "")
		config := &mogo.Config{
			ConnectionString: connectionString,
			Database:         dbName,
		}
		mongoConnection, err := mogo.Connect(config)
		if err != nil {
			log.Fatal(err)
		} else {
			mogo.ModelRegistry.Register(entity.Movie{})
			mogo.ModelRegistry.Register(entity.User{})

			return mongoConnection
		}
	}
	return mongoConnection
}
