package entity

import (
	"github.com/goonode/mogo"
)

//Movie struct is to handle movie data
type Movie struct {
	mogo.DocumentModel `bson:",inline" coll:"movies"`
	Title              string   `json:"title"`
	Description        string   `json:"description"`
	Price              float32  `json:"price"`
	ReleaseYear        int      `json:"release_year"`
	CreatedBy          UserInfo `json:"created_by"`
	UpdatedBy          UserInfo `json:"updated_by"`
}

//UserInfo struct is to handle userinfo related to movies
type UserInfo struct {
	Name string `json:"name"`
}

func init() {
	mogo.ModelRegistry.Register(Movie{})
}
