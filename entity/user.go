package entity

import (
	jwt "github.com/form3tech-oss/jwt-go"
	"github.com/goonode/mogo"
	"goMovieApp/utils"
	"time"
)

//User struct is to handle user data
type User struct {
	mogo.DocumentModel `bson:",inline" coll:"users"`
	Email              string `idx:"{email},unique" json:"email" binding:"required"`
	Password           string `json:"password" binding:"required"`
	Name               string `json:"name"`
}

//GetJwtToken returns jwt token with user email claims
func (user *User) GetJwtToken(jwttype string) (string, error) {
	if jwttype == "refresh" {
		expiry := time.Now().Add(time.Hour * 24 * 7).Unix()

		token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
			"email": string(user.Email),
			"name":  string(user.Name),
			"exp":   expiry,
		})
		secretKey := utils.EnvVar("APP_SECRET_REFRESH", "")
		tokenString, err := token.SignedString([]byte(secretKey))
		return tokenString, err
	}
	expiry := time.Now().Add(time.Hour * 1).Unix()

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"email": string(user.Email),
		"name":  string(user.Name),
		"exp":   expiry,
	})
	secretKey := utils.EnvVar("APP_SECRET", "")
	tokenString, err := token.SignedString([]byte(secretKey))
	return tokenString, err
}

func init() {
	mogo.ModelRegistry.Register(User{})
}
