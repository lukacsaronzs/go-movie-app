package service

import (
	"errors"
	"github.com/goonode/mogo"
	"goMovieApp/db"
	"goMovieApp/entity"
	"labix.org/v2/mgo/bson"
)

//UserService is to handle user relation db query
type UserService struct{}

//Create is to register new user
func (userservice UserService) Create(user *(entity.User)) (*entity.User, error) {
	conn := db.GetConnection()
	defer conn.Session.Close()

	doc := mogo.NewDoc(entity.User{}).(*(entity.User))
	err := doc.FindOne(bson.M{"email": user.Email}, doc)
	if err == nil {
		return &entity.User{}, errors.New("Already Exist")
	}
	userModel := mogo.NewDoc(user).(*(entity.User))
	err = mogo.Save(userModel)
	if vErr, ok := err.(*mogo.ValidationError); ok {
		return userModel, vErr
	}
	return userModel, err
}

//FindByEmail user by email
func (userservice UserService) FindByEmail(email string) (*entity.User, error) {
	conn := db.GetConnection()
	defer conn.Session.Close()

	user := new(entity.User)
	user.Email = email
	doc := mogo.NewDoc(entity.User{}).(*(entity.User))
	err := doc.FindOne(bson.M{"email": user.Email}, doc)

	if err != nil {
		return nil, err
	}
	return doc, nil

}

//FindByID user by email
func (userservice UserService) FindByID(ID string) (*entity.User, error) {
	conn := db.GetConnection()
	defer conn.Session.Close()
	user := entity.User{}
	err := conn.Collection("users").Find(bson.M{"_id": bson.ObjectIdHex(ID)}).
		One(&user)
	return &user, err
}
