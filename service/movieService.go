package service

import (
	"errors"
	"github.com/globalsign/mgo/bson"
	"github.com/goonode/mogo"
	"goMovieApp/db"
	"goMovieApp/entity"
)

//MovieService is to handle user relation db query
type MovieService struct{}

//AddMovie adds a new movie entry to the database
func (movieService MovieService) AddMovie(movie *(entity.Movie)) (entity.Movie, error) {
	conn := db.GetConnection()
	defer conn.Session.Close()

	movieModel := mogo.NewDoc(movie).(*(entity.Movie))

	err := mogo.Save(movieModel)
	if vErr, ok := err.(*mogo.ValidationError); ok {
		return *movie, vErr
	}
	return *movieModel, err
}

//GetAllMovies returns all movie entries from the database
func (movieService MovieService) GetAllMovies(offset int, limit int) ([](*entity.Movie), int, int, int) {
	conn := db.GetConnection()
	defer conn.Session.Close()
	movies := make([](*entity.Movie), limit)
	doc := mogo.NewDoc(entity.Movie{}).(*entity.Movie)

	iter := doc.Find(nil).Paginate(limit).Iter()
	iter.NextPage(&movies)
	for i := 0; i < offset; i++ {
		iter.NextPage(&movies)
	}

	return movies, iter.Pagination.Page - 1, iter.Pagination.Pages, iter.Pagination.T
}

//FindByID returns a single movie entry from the database by id
func (movieService MovieService) FindByID(ID string) (entity.Movie, error) {
	conn := db.GetConnection()
	defer conn.Session.Close()

	movie := entity.Movie{}
	if bson.IsObjectIdHex(ID) {
		movieID := bson.ObjectIdHex(ID)

		err := conn.Collection("movies").Find(bson.M{"_id": movieID}).One(&movie)
		var ErrNotFound error = nil
		if err != nil {
			ErrNotFound = errors.New("Error in retrieving movie")
		}
		return movie, ErrNotFound
	}

	return movie, errors.New("No record with given id")

}

//DeleteMovie deletes a  movie entry from the database
func (movieService MovieService) DeleteMovie(id string) (*entity.Movie, error) {

	movie, err := movieService.FindByID(id)
	if err != nil {

		return &movie, errors.New("No movie with given id")
	}
	conn := db.GetConnection()
	defer conn.Session.Close()

	movieModel := mogo.NewDoc(movie).(*(entity.Movie))
	err2 := mogo.Remove(movieModel)
	if vErr, ok := err2.(*mogo.ValidationError); ok {
		return movieModel, vErr
	}
	return movieModel, err2
}

//UpdateMovie updates a  movie entry in the database identified by id
func (movieService MovieService) UpdateMovie(id string, newMovie *(entity.Movie)) (*entity.Movie, error) {

	movie, err := movieService.FindByID(id)
	if err != nil {

		return &movie, errors.New("No movie with given id")
	}
	conn := db.GetConnection()
	defer conn.Session.Close()

	movie.Title = newMovie.Title
	movie.Description = newMovie.Description
	movie.Price = newMovie.Price
	movie.ReleaseYear = newMovie.ReleaseYear
	movie.UpdatedBy = newMovie.UpdatedBy

	newMovieModel := mogo.NewDoc(movie).(*(entity.Movie))
	err2 := mogo.Save(newMovieModel)
	if vErr, ok := err2.(*mogo.ValidationError); ok {
		return newMovieModel, vErr
	}
	return newMovieModel, err2

}
