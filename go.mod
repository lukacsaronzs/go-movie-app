module goMovieApp

go 1.15

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/form3tech-oss/jwt-go v3.2.2+incompatible
	github.com/friendsofgo/errors v0.9.2
	github.com/fsnotify/fsnotify v1.4.9 // indirect
	github.com/gin-gonic/gin v1.6.3
	github.com/globalsign/mgo v0.0.0-20181015135952-eeefdecb41b8
	github.com/go-bongo/go-dotaccess v0.0.0-20190924013105-74ea4f4ca4eb // indirect
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gobeam/mongo-go-pagination v0.0.2
	github.com/gofiber/fiber v1.14.6
	github.com/gofiber/fiber/v2 v2.2.1
	github.com/gofiber/jwt/v2 v2.1.0
	github.com/goonode/mogo v0.0.0-20181028112152-10c38e9be609
	github.com/joho/godotenv v1.3.0
	github.com/kat-co/vala v0.0.0-20170210184112-42e1d8b61f12
	github.com/magiconair/properties v1.8.4 // indirect
	github.com/mitchellh/mapstructure v1.4.0 // indirect
	github.com/oleiade/reflections v1.0.1 // indirect
	github.com/pelletier/go-toml v1.8.1 // indirect
	github.com/smartystreets/goconvey v1.6.4
	github.com/spf13/afero v1.4.1 // indirect
	github.com/spf13/cobra v1.1.1
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/spf13/viper v1.7.1
	github.com/volatiletech/null/v8 v8.1.0
	github.com/volatiletech/randomize v0.0.1
	github.com/volatiletech/sqlboiler/v4 v4.3.1
	github.com/volatiletech/strmangle v0.0.1
	go.mongodb.org/mongo-driver v1.4.3
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
	golang.org/x/sys v0.0.0-20201126233918-771906719818 // indirect
	golang.org/x/text v0.3.4 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	gopkg.in/ini.v1 v1.62.0 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
	gorm.io/driver/mysql v1.0.3
	gorm.io/driver/sqlite v1.1.4 // indirect
	gorm.io/gorm v1.20.7
	labix.org/v2/mgo v0.0.0-20140701140051-000000000287
)
