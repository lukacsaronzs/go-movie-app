package controller

import (
	//"fmt"
	//jwt "github.com/form3tech-oss/jwt-go"
	"fmt"
	jwt "github.com/form3tech-oss/jwt-go"
	"github.com/gofiber/fiber/v2"
	"goMovieApp/entity"
	"goMovieApp/service"
	"log"
	"regexp"

	"golang.org/x/crypto/bcrypt"
)

//UserController is for auth logic
type UserController struct {
	UserService *service.UserService
}

//GenerateNewToken generates a new access token based on an existing refresh token
func (controller *UserController) GenerateNewToken(ctx *fiber.Ctx) error {

	u := ctx.Locals("user").(*jwt.Token)
	claims := u.Claims.(jwt.MapClaims)
	userEmail := claims["email"].(string)

	user, errf := controller.UserService.FindByEmail(userEmail)
	if errf != nil {
		return ctx.Status(401).JSON(fiber.Map{
			"error": "Invalid or expired JWT token.",
		})

	}
	fmt.Println(u)

	token, err := user.GetJwtToken("login")
	if err != nil {
		return ctx.Status(500).JSON(err.Error())

	}
	return ctx.Status(200).JSON(fiber.Map{
		"data": fiber.Map{
			"token": token,
		},
	})
	/*
		return ctx.Status(200).JSON(fiber.Map{
			"data": fiber.Map{
				"ok": "ok",
			},
		})
	*/
}

//Login is to process login request
func (controller *UserController) Login(c *fiber.Ctx) error {

	type credentialsInfo struct {
		Email    string `json:"email" binding:"required"`
		Password string `json:"password" binding:"required"`
	}

	var credentials credentialsInfo
	if err := c.BodyParser(&credentials); err != nil {
		return c.Status(400).JSON(fiber.Map{
			"error": "Missing data from request body.",
		})

	}
	if credentials.Email == "" || credentials.Password == "" {
		return c.Status(400).JSON(fiber.Map{
			"error": "Missing data from request body.",
		})

	}
	userData := entity.User{}
	userData.Email = credentials.Email
	userData.Password = credentials.Password

	user, errf := controller.UserService.FindByEmail(userData.Email)
	if errf != nil {
		return c.Status(401).JSON(fiber.Map{
			"error": "Invalid login credentials.",
		})

	}

	err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(userData.Password))
	if err != nil {
		return c.Status(401).JSON(fiber.Map{
			"error": "Invalid login credentials.",
		})

	}

	token, err := user.GetJwtToken("login")
	if err != nil {
		return c.Status(500).JSON(err.Error())

	}
	refreshToken, err2 := user.GetJwtToken("refresh")
	if err2 != nil {
		return c.Status(500).JSON(err.Error())

	}

	return c.Status(200).JSON(fiber.Map{
		"data": fiber.Map{
			"token":         token,
			"refresh_token": refreshToken,
		},
	})
}

type signupInfo struct {
	Email    string `json:"email" binding:"required"`
	Password string `json:"password" binding:"required"`
	Name     string `json:"name"`
}

//Register is for user signup
func (controller *UserController) Register(c *fiber.Ctx) error {

	credentials := new(signupInfo)
	if err := c.BodyParser(credentials); err != nil {
		return c.Status(400).JSON(fiber.Map{
			"error": "Missing data from request body.",
		})

	}
	if credentials.Email == "" || credentials.Name == "" || credentials.Password == "" {
		return c.Status(400).JSON(fiber.Map{
			"error": "Missing data from request body.",
		})

	}

	if !ValidateUser(credentials) {
		return c.Status(422).JSON(fiber.Map{
			"error": "Invalid user data.",
		})
	}
	u, errf := controller.UserService.FindByEmail(credentials.Email)
	if errf == nil {
		return c.Status(400).JSON(fiber.Map{
			"error": "User with given email address: " + u.Email + " already exists.",
		})
	}

	user := entity.User{}
	user.Email = credentials.Email
	hash, err := bcrypt.GenerateFromPassword([]byte(credentials.Password), bcrypt.MinCost)
	if err != nil {
		log.Fatal(err)
		return c.Status(500).JSON(fiber.Map{
			"error": "Could not save new user.",
		})
	}

	user.Password = string(hash)
	user.Name = credentials.Name

	createdUser, err := controller.UserService.Create(&user)
	if err != nil {
		return c.Status(500).JSON(fiber.Map{
			"error": "Could not save new user.",
		})
	}

	return c.Status(201).JSON(fiber.Map{
		"data": fiber.Map{
			"_id":   createdUser.ID,
			"name":  createdUser.Name,
			"email": createdUser.Email,
		},
	})

}

func validateEmail(email string) bool {
	match, _ := regexp.MatchString(`^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$`, email)
	return match
}

//ValidateUser checks constraints on fields of a movie entity
func ValidateUser(user *signupInfo) bool {
	if len(user.Name) < 5 {
		return false
	}
	if len(user.Password) < 5 {

		return false
	}

	return validateEmail(user.Email)
}
