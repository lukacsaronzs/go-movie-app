package controller

import (
	"errors"
	jwt "github.com/form3tech-oss/jwt-go"
	"github.com/gofiber/fiber/v2"
	"goMovieApp/entity"
	"goMovieApp/service"
	"time"

	"strconv"
)

//MovieController handles the requests on the /api/movies routes
type MovieController struct {
	MovieService *service.MovieService
	UserService  *service.UserService
}

//MovieData stores attributes of a movie entity from client side
type MovieData struct {
	Title       string   `json:"title" binding:"required"`
	Description string   `json:"description" binding:"required"`
	Price       *float32 `json:"price" binding:"required"`
	ReleaseYear *int     `json:"release_year" binding:"required"`
}

type responseAll struct {
	TotalItems  int
	Movies      []*entity.Movie
	TotalPages  int
	CurrentPage int
}

//ValidateMovie checks constraints on fields of a movie entity
func ValidateMovie(movie *entity.Movie) bool {
	if len(movie.Title) < 5 || len(movie.Title) > 60 {
		return false
	}
	if len(movie.Description) < 5 || len(movie.Description) > 100 {
		return false
	}
	if movie.Price < 0 {
		return false
	}
	if movie.ReleaseYear < 1900 || movie.ReleaseYear > time.Now().Year() {
		return false
	}
	return true
}

//ValidatePagination returns and verifies the integer valus from the query parameters page and size used for paginating the movies
func ValidatePagination(page string, size string) (int, int, error) {
	pageInt, err := strconv.Atoi(page)
	if err != nil {
		return 0, 0, err

	}
	sizeInt, err := strconv.Atoi(size)
	if err != nil {
		return 0, 0, err
	}
	if pageInt < 0 || sizeInt < 0 {
		return 0, 0, errors.New("Page and size must be positive integers")
	}
	return pageInt, sizeInt, nil

}

//GetAllMovies returns all movie entries from the database
func (controller MovieController) GetAllMovies(ctx *fiber.Ctx) error {

	page := ctx.Query("page", "0")
	size := ctx.Query("size", "3")

	pageInt, sizeInt, err := ValidatePagination(page, size)
	if err != nil {
		return ctx.Status(422).JSON(fiber.Map{
			"error": "Page and size must be positive integers.",
		})
	}

	movies, current, total, n := controller.MovieService.GetAllMovies(pageInt, sizeInt)
	res := responseAll{}
	res.Movies = movies
	res.CurrentPage = current
	res.TotalPages = total
	res.TotalItems = n
	return ctx.Status(200).JSON(fiber.Map{"data": res})

}

//GetMovie returns a single movie entry from the database by id
func (controller MovieController) GetMovie(ctx *fiber.Ctx) error {
	id := ctx.Params("id")
	movie, err := controller.MovieService.FindByID(id)
	if err != nil {
		if err.Error() == "Error in retrieving movie" {
			return ctx.Status(500).JSON(fiber.Map{"error": "Error in retrieving movie."})
		}
		return ctx.Status(404).JSON(fiber.Map{"error": "No movie with given id: " + id + "."})

	}

	return ctx.Status(200).JSON(fiber.Map{"data": movie})

}

//AddMovie adds a new movie entry to the database
func (controller MovieController) AddMovie(ctx *fiber.Ctx) error {

	movieData := new(MovieData)
	if err := ctx.BodyParser(movieData); err != nil {
		return ctx.Status(400).JSON(fiber.Map{
			"error": "Missing data from request body.",
		})
	}
	if movieData.Title == "" || movieData.Description == "" || movieData.Price == nil || movieData.ReleaseYear == nil {
		return ctx.Status(400).JSON(fiber.Map{
			"error": "Missing data from request body.",
		})
	}

	user := ctx.Locals("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	userName := claims["name"].(string)

	userInfo := entity.UserInfo{}
	userInfo.Name = userName

	movie := entity.Movie{}
	movie.Title = movieData.Title
	movie.Description = movieData.Description
	movie.Price = *movieData.Price
	movie.ReleaseYear = *movieData.ReleaseYear
	movie.CreatedBy = userInfo
	movie.UpdatedBy = userInfo

	if !ValidateMovie(&movie) {
		return ctx.Status(422).JSON(fiber.Map{
			"error": "Invalid movie data.",
		})
	}

	resMovie, errf := controller.MovieService.AddMovie(&movie)
	if errf != nil {
		return ctx.Status(500).JSON(fiber.Map{
			"error": "Could not update movie.",
		})
	}

	return ctx.Status(201).JSON(fiber.Map{
		"data": resMovie,
	})

}

//DeleteMovie deletes a  movie entry from the database
func (controller MovieController) DeleteMovie(ctx *fiber.Ctx) error {

	id := ctx.Params("id")

	delMovie, err2 := controller.MovieService.DeleteMovie(id)
	if err2 != nil {
		if err2.Error() == "No movie with given id" {
			return ctx.Status(404).JSON(fiber.Map{
				"error": "No movie with given id: " + id + ".",
			})
		}
		return ctx.Status(500).JSON(fiber.Map{
			"error": "Could not delete movie.",
		})
	}

	return ctx.Status(200).JSON(fiber.Map{
		"data": delMovie,
	})

}

//UpdateMovie updates a  movie entry in the database identified by id
func (controller MovieController) UpdateMovie(ctx *fiber.Ctx) error {

	movieData := new(MovieData)
	if err := ctx.BodyParser(movieData); err != nil {
		return ctx.Status(400).JSON(fiber.Map{
			"error": "Missing data from request body.",
		})

	}
	if movieData.Title == "" || movieData.Description == "" || movieData.Price == nil || movieData.ReleaseYear == nil {
		return ctx.Status(400).JSON(fiber.Map{
			"error": "Missing data from request body.",
		})
	}
	user := ctx.Locals("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	userName := claims["name"].(string)

	userInfo := entity.UserInfo{}
	userInfo.Name = userName
	newMovie := entity.Movie{}
	newMovie.Title = movieData.Title
	newMovie.Description = movieData.Description
	newMovie.Price = *movieData.Price
	newMovie.ReleaseYear = *movieData.ReleaseYear
	newMovie.UpdatedBy = userInfo

	if !ValidateMovie(&newMovie) {
		return ctx.Status(422).JSON(fiber.Map{
			"error": "Invalid movie data.",
		})
	}
	id := ctx.Params("id")

	resMovie, errf := controller.MovieService.UpdateMovie(id, &newMovie)
	if errf != nil {
		if errf.Error() == "No movie with given id" {
			return ctx.Status(404).JSON(fiber.Map{
				"error": "No movie with given id: " + id + ".",
			})
		}
		return ctx.Status(500).JSON(fiber.Map{
			"error": "Could not update movie.",
		})
	}

	return ctx.Status(200).JSON(fiber.Map{
		"data": resMovie,
	})

}
