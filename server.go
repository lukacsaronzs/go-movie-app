package main

//go:generate sqlboiler --wipe mysql

import (
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"goMovieApp/controller"
	"goMovieApp/routers"

	"goMovieApp/service"
	"goMovieApp/utils"

	"log"
)

func initDatabase() {

}

func main() {

	movieService := new(service.MovieService)
	userService := new(service.UserService)
	movieController := new(controller.MovieController)
	userController := new(controller.UserController)
	userController.UserService = userService
	movieController.MovieService = movieService
	movieController.UserService = userService

	app := fiber.New()

	app.Use(logger.New())
	app.Use(cors.New())
	app.Use(cors.New(cors.Config{
		AllowOrigins: "*",
		AllowHeaders: "Origin, X-Requested-With, Content-Type, Accept, Authorization",
		AllowMethods: "GET, POST, PUT, PATCH, DELETE, OPTIONS",
	}))

	routers.SetupUserRoutes(app, userController)

	routers.SetupMovieRoutes(app, movieController)

	log.Fatal(app.Listen(utils.EnvVar("SERVER_PORT", ":8080")))

}
