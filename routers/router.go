package routers

import (
	"github.com/gofiber/fiber/v2"
	jwtware "github.com/gofiber/jwt/v2"
	"goMovieApp/controller"
	"goMovieApp/utils"
)

//SetupMovieRoutes creates api routes related to the movie entity
func SetupMovieRoutes(app *fiber.App, movieController *controller.MovieController) {
	movies := app.Group("/api/movies", jwtware.New(jwtware.Config{
		SigningKey: []byte(utils.EnvVar("APP_SECRET", "")),
	}))
	movies.Get("/", movieController.GetAllMovies)
	movies.Get("/:id", movieController.GetMovie)
	movies.Post("/", movieController.AddMovie)
	movies.Delete("/:id", movieController.DeleteMovie)
	movies.Put("/:id", movieController.UpdateMovie)

}

//SetupUserRoutes creates api routes related to the movie entity
func SetupUserRoutes(app *fiber.App, userController *controller.UserController) {
	app.Post("/api/register", userController.Register)
	app.Post("/api/login", userController.Login)
	token := app.Group("api/token", jwtware.New(jwtware.Config{
		SigningKey: []byte(utils.EnvVar("APP_SECRET_REFRESH", "")),
	}))

	token.Get("/", userController.GenerateNewToken)

}
